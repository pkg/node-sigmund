# Installation
> `npm install --save @types/sigmund`

# Summary
This package contains type definitions for sigmund (https://github.com/isaacs/sigmund#readme).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sigmund

Additional Details
 * Last updated: Thu, 24 Aug 2017 17:15:34 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by BendingBender <https://github.com/BendingBender>.
